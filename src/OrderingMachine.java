import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrderingMachine {

    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = textBox.getText();
            textBox.setText(food + " " + price + " yen");
            String newText = textBox.getText();
            textBox.setText(currentText + newText + "\n");
            JOptionPane.showMessageDialog(null, "Thank you. Order for " + food +" received.");

            String currentPrice = Label.getText();
            Label.setText( price + " yen");
            String intPrice =currentPrice.replaceAll("[^0-9]", "");
            int newPrice=Integer.parseInt(intPrice)+ price;
            Label.setText( newPrice + " yen");


        }
    }

    private JPanel root;
    private JButton kamaageButton;
    private JButton kakeButton;
    private JButton nikuButton;
    private JButton mentaiButton;
    private JButton oroshiButton;
    private JButton torotamaButton;
    private JTextArea textBox;
    private JButton Check;
    private JLabel Total;
    private JLabel Label;

    public OrderingMachine() {
        kamaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kamaageudon",340);
            }
        });
        kakeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kakeudon",390);
            }
        });
        nikuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakitatenikuudon",750);
            }
        });
        mentaiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mentaikamatamaudon",560);
            }
        });
        oroshiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Oroshisyoyuudon",490);
            }
        });
        torotamaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Torotamaudon",560);
            }
        });
        Check.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation==0) {
                    String finalPrice = Label.getText();
                    JOptionPane.showMessageDialog(null, "Thank you! " +
                            "The total price is " + finalPrice + ".");
                    textBox.setText("");
                    Label.setText("0 yen");
                }
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("OrderingMachine");
        frame.setContentPane(new OrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
