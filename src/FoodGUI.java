import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton kamaageButton;
    private JButton kakeButton;
    private JButton nikuButton;
    private JTextArea textBox;
    private JButton mentaiButton;
    private JButton oroshiButton;
    private JButton torotamaButton;
    private JButton checkOutButton;
    private JLabel yen;
    private JButton cancelButton;


    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = textBox.getText();
            textBox.setText(food + " " + price + " yen");
            String newText = textBox.getText();
            textBox.setText(currentText + newText + "\n");
            JOptionPane.showMessageDialog(null,"Order for " + food + " received.");

            String currentPrice = yen.getText();
            yen.setText( price + "");
            String intPrice =currentPrice.replaceAll("[^0-9]", "");
            int newPrice=Integer.parseInt(intPrice)+ price;
            yen.setText( newPrice + "");

        }
    }
    public FoodGUI() {

        kamaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon-kamaage.jpg")
        ));
        kakeButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon-kake.jpg")
        ));
        nikuButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon-yakitate-niku.jpg")
        ));
        mentaiButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon-mentai-kamatama.jpg")
        ));
        oroshiButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon-oroshishoyu.jpg")
        ));
        torotamaButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon-torotama.jpg")
        ));

        kamaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kamaage-udon",340); }
        });
        kakeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Kake-udon",390);
            }
        });
        nikuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Yakitate-niku-udon",750);
            }
        });
        mentaiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mentai-kamatama-udon",580);

            }
        });
        oroshiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Oroshishoyu-udon",490);

            }
        });
        torotamaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Torotama-udon",560);

            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation==0) {
                    int confirmation2 = JOptionPane.showConfirmDialog(
                            null,
                            "Do you have Udon-huda?",
                            "discount confirmation",
                            JOptionPane.YES_NO_OPTION
                    );
                    if(confirmation2==0) {
                        String Bill = yen.getText();
                        int num =0;
                        num = Integer.parseInt(Bill);
                        num -= 300;
                        System.out.println(num);
                        String newBill = String.valueOf(num);
                        yen.setText(newBill);

                        JOptionPane.showMessageDialog(null, "Thank you! 300 yen discount.\n\n" +
                                "The total price is " + newBill + " yen.");
                        textBox.setText("");
                        yen.setText("0");

                    }
                    else {
                        String bill = yen.getText();
                        JOptionPane.showMessageDialog(null, "Thank you! " +
                                "The total price is " + bill + " yen.");
                        textBox.setText("");
                        yen.setText("0");
                    }
                }
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to cancel your order?",
                        "Cancel confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation==0) {
                    String bill = yen.getText();
                    JOptionPane.showMessageDialog(null, "Order canceled." );
                    textBox.setText("");
                    yen.setText("0");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

